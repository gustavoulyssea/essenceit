<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuoteController extends Controller
{
    public function getQuote($from=0,$to=0){
        $contents = Storage::get('routes.csv');
        $routes = explode(PHP_EOL,$contents);

        // calculate Vertices
        $vertices = [];
        foreach($routes as $route){
            if(!$route) continue;
            $columns = explode(",",$route);
            $encontrado=0;
            foreach($vertices as $vertice){
                if($columns[0]==$vertice) $encontrado=1;
            }
            if(!$encontrado) $vertices[]=$columns[0];
            $encontrado=0;
            foreach($vertices as $vertice){
                if($columns[1]==$vertice) $encontrado=1;
            }
            if(!$encontrado) $vertices[]=$columns[1];
        }
        $V = count($vertices); // Number of vertices in graph
        $E = count($routes); // Number of edges in graph

        // Every edge has three values (u, v, w) where
        // the edge is from vertex u to v. And weight
        // of the edge is w.
//        $graph = array( array( 0, 1, -1 ), array( 0, 2, 4 ),
//            array( 1, 2, 3 ), array( 1, 3, 2 ),
//            array( 1, 4, 2 ), array( 3, 2, 5),
//            array( 3, 1, 1), array( 4, 3, -3 ) );

        // converter para array so com numeros
        $routes_num = [];
        foreach($routes as $route){
            if(!$route) continue;
            $columns = explode(",",$route);
            for($i=0;$i<count($vertices);$i++){
                if($columns[0]==$vertices[$i]) $coluna1 = $i;
                if($columns[1]==$vertices[$i]) $coluna2 = $i;
            }
            $routes_num[] = array($coluna1,$coluna2,$columns[2]);
        }

        $this->BellmanFord($routes_num, $V, $E, 0);

    }
        // A PHP program for Bellman-Ford's single
    // source shortest path algorithm.

    // The main function that finds shortest
    // distances from src to all other vertices
    // using Bellman-Ford algorithm. The function
    // also detects negative weight cycle
    // The row graph[i] represents i-th edge with
    // three values u, v and w.
    public function BellmanFord($graph, $V, $E, $src)
    {
        // Initialize distance of all vertices as 0.
        $dis = array();
        for ($i = 0; $i < $V; $i++)
            $dis[$i] = PHP_INT_MAX;

        // initialize distance of source as 0
        $dis[$src] = 0;

        // Relax all edges |V| - 1 times. A simple
        // shortest path from src to any other
        // vertex can have at-most |V| - 1 edges
        for ($i = 0; $i < $V - 1; $i++)
        {
            for ($j = 0; $j < $E; $j++)
            {
                if ($dis[$graph[$j][0]] + $graph[$j][2] <
                    $dis[$graph[$j][1]])
                    $dis[$graph[$j][1]] = $dis[$graph[$j][0]] +
                        $graph[$j][2];
            }
        }

        // check for negative-weight cycles.
        // The above step guarantees shortest
        // distances if graph doesn't contain
        // negative weight cycle. If we get a
        // shorter path, then there is a cycle.
        for ($i = 0; $i < $E; $i++)
        {
            $x = $graph[$i][0];
            $y = $graph[$i][1];
            $weight = $graph[$i][2];
//            if ($dis[$x] != PHP_INT_MAX &&
//                $dis[$x] + $weight < $dis[$y])
//                echo "Graph contains negative weight cycle \n";
        }

        echo "Vertex Distance from Source \n";
        for ($i = 0; $i < $V; $i++)
            echo $i, "\t\t", $dis[$i], "\n";
    }
}
